Demo = Ember.Application.create
    ApplicationController: Ember.Controller.extend()
    ApplicationView: Ember.View.extend
        templateName: 'application'

router = Ember.Router.create
    root: Ember.Route.extend
        index: Ember.Route.extend
            route: '/'
            redirectsTo: 'things'

        things: Ember.Route.extend
            route: '/things'
            enter: -> console.log "Entering route /things"
            connectOutlets: (router) -> router.get('applicationController').connectOutlet 'things'

        foos: Ember.Route.extend
            route: '/foos'
            enter: -> console.log "Entering route /foos"
            connectOutlets: (router) -> router.get('applicationController').connectOutlet 'foos'

Demo.Thing = Ember.Object.extend
    name: null

Demo.things = Ember.ArrayController.create
    content: []

Demo.ThingsView = Ember.View.extend
    templateName: 'things'

Demo.FoosView = Ember.View.extend
    templateName: 'foos'

Demo.initialize(router)
